//
//  main.c
//  DiskUsage
//
//  Created by Rajiv Navada on 12/30/12.
//  Copyright (c) 2012 Chalktips, LLC. All rights reserved.
//

#include "path.h"

int main(int argc, char *argv[])
{
    CT::Path *pth = NULL;
    
    if (argc == 1) {
        char *curpath = getcwd(NULL, 0);
        pth = new CT::Path(curpath);
        delete [] curpath;
    } else {
        argv++;
        pth = new CT::Path(*argv);
    }
    
    const char *root = pth->get_root();
    const char *type = pth->get_type();
    off_t size       = pth->get_size();
    
    std::printf("%s - %s - %lld bytes\n", root, type, size);
    
    // // Copy
    // CT::Path pth2 = *pth;
    
    // root = pth2.get_root();
    // type = pth2.get_type();
    // size = pth2.get_size();
    // std::printf("%s - %s - %lld bytes\n", root, type, size);
    
    // Free memory allocated for path
    delete pth;
    
    return 0;
}
