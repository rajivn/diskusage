//
//  path.h
//  DiskUsage
//
//  Created by Rajiv Navada on 12/27/12.
//  Copyright (c) 2012 Chalktips, LLC. All rights reserved.
//

#ifndef __DiskUsage__path__
#define __DiskUsage__path__

#include <iostream>
#include <map>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

#ifndef PATH_SEP
#define PATH_SEP '/'
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#define TYPE_DIR            "directory"
#define TYPE_FILE           "file"
#define TYPE_LINK           "link"
#define TYPE_UNSUPPORTED    "unsupported"
#define IS_UNSUPPORTED(m)   (!S_ISDIR(m) && !S_ISREG(m) && !S_ISLNK(m))
#define IS_SUPPORTED(m)     (!IS_UNSUPPORTED(m))
#define TYPE_REPR(m)        ((S_ISDIR(m)) ? TYPE_DIR : ((S_ISREG(m)) ? TYPE_FILE : ((S_ISLNK(m)) ? TYPE_LINK : TYPE_UNSUPPORTED)))

#if DEBUG
#define PRINTLN(x, ...) \
    do { \
        printf("%s [%s:%d] ", __func__, __FILE__, __LINE__); \
        printf(x,##__VA_ARGS__); \
        printf("\n"); \
    } while(0)
#else
#define PRINTLN(x, ...)
#endif

namespace CT {
    /*!
     * This class defines a path for which info will be retrieved
     */
    class Path {
    public:
        // PathMapping Class
        class PathMapping : public std::map<char *, Path *> {
        public:
            // Default constructor
            PathMapping() {};
            
            // Copy constructor
            PathMapping(const PathMapping &);
            
            // Destructor
            ~PathMapping();
            
            std::pair<iterator, bool> add_path(Path *);
        };

    protected:
        // Fields
        mode_t      mode;
        off_t       size;
        bool        scan_finished;
        bool        calc_finished;
        char        *root;
        PathMapping *subpaths;
        
        // Methods
        void add_subpath(const char *path, const struct stat *st);
        void set_root(const char *root);
        void scan_path();
        void calc_size();
        void destroy_root();
        void destroy_subpaths();
        void copy_from(const Path &path);
        
    public:
        // Default constructor
        Path(const char *root, const struct stat *st = NULL);
        
        // Copy constructor
        Path(const Path &path);
        
        // We need the assignment operator to handle assignments
        Path& operator=(const Path &path);
        
        ~Path();
        
        /*!
         * @return String representing the root path on which this object is operating.
         */
        const char *get_root() const;
        
        /*!
         * @return String representing the type of object this path is representing.
         */
        const char *get_type() const;
        
        /*!
         * @return The subpaths map as determined by scanning the root path.
         */
        PathMapping *get_subpaths();
        
        /*!
         * @return Size of the path pointed to by this object
         */
        off_t get_size();
    };
}

#endif /* defined(__DiskUsage__path__) */
