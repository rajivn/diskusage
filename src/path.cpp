//
//  path.cpp
//  DiskUsage
//
//  Created by Rajiv Navada on 12/27/12.
//  Copyright (c) 2012 Chalktips, LLC. All rights reserved.
//
#include "path.h"

//--------------------------------------------------
// PRIVATE METHODS
//--------------------------------------------------

// Add a subpath to this path
void CT::Path::add_subpath(const char *path, const struct stat *st)
{
    // TODO: we can raise an exception if an attempt is made to add a subpath to a file
    this->subpaths->add_path(new Path(path, st));
}

// Set a path as the root path this object is responsible for
void CT::Path::set_root(const char *root)
{
    this->destroy_root();
    if (!root) return;
    // Allocate new memory for root path
    char *newp = new char[strlen(root) + 1];
    this->root = strcpy(newp, root);
}

// Scan all paths within the root path and prepare a map
void CT::Path::scan_path()
{
    // If there are no subpaths, or the subpaths has already been initialized, simply return
    if (!this->subpaths || this->scan_finished) return;
    
    // Declare variables
    const char *dirname     = this->root;
    struct dirent *p_entry  = NULL;
    struct stat st          = {0};
    DIR *p_dir              = NULL;
    char *path              = NULL;
    off_t pathlen           = 0;
    off_t newlen            = 0;
    
    // Open the directory and try to inspect
    if ((p_dir = opendir(dirname)) == NULL) {
        PRINTLN("Couldn't open path %s", dirname);
        return;
    }
    
    // Read one entry at a time from the directory stream
    while ((p_entry = readdir(p_dir)) != NULL) {
        // Ignore the standard . and .. entries
        if (strcmp(p_entry->d_name, "..") == 0 || strcmp(p_entry->d_name, ".") == 0) {
            continue;
        }
        
        // Allocate memory to hold pathname
        // NOTE: we have a path separator hence the additional 1
        newlen = strlen(dirname) + 1 + strlen(p_entry->d_name) + 1;
        if (newlen > pathlen) {
            delete [] path;
            // We double the new length so that we don't have to allocate frequently
            newlen *= 2;
            path = new char[newlen];
            pathlen = newlen;
        } else {
            // Reset the path
            memset(path, 0, pathlen);
        }
        
        // Construct path
        if (strlen(strrchr(dirname, PATH_SEP)) == 1) {
            std::sprintf(path, "%s%s", dirname, p_entry->d_name);
        } else {
            std::sprintf(path, "%s%c%s", dirname, PATH_SEP, p_entry->d_name);
        }
        
        // Get stat
        if (lstat(path, &st)) {
            PRINTLN("Could not stat the path %s", path);
        } else if (IS_SUPPORTED(st.st_mode)) {
            this->add_subpath(path, &st);
        } else {
            PRINTLN("%s is unsupported", path);
        }
    }
    
    closedir(p_dir);
    delete [] path;
    
    this->scan_finished = true;
}

// Calculate the size of the path
void CT::Path::calc_size()
{
    // There is nothing to do if there are no subpaths or the calc is already done.
    if (!this->subpaths || this->calc_finished) return;
    
    // Iterate over the subpaths to calculate size.
    std::map<char *, Path *>::iterator it;
    for (it = this->subpaths->begin(); it != this->subpaths->end(); it++) {
        this->size += it->second->get_size();
    }
    this->calc_finished = true;
}

// Destroy the root path string
void CT::Path::destroy_root()
{
    // Free the existing root
    if (this->root) {
        delete [] this->root;
        this->root = NULL;
    }
}

// Destroy the subpaths mapping
void CT::Path::destroy_subpaths()
{
    // Simply call delete. The PathMapping class will handle the nitty-gritty :)
    if (this->subpaths) {
        delete this->subpaths;
        this->subpaths = NULL;
    }
}

// Copy values from the passed in path into the current object
void CT::Path::copy_from(const CT::Path &path)
{
    // Copy the scalars
    this->size          = path.size;
    this->mode          = path.mode;
    this->scan_finished = path.scan_finished;
    this->calc_finished = path.calc_finished;
    // Copy the subpaths map
    if (path.subpaths) {
        // Remove whatever is there currently
        this->destroy_subpaths();
        // Construct a new subpaths mapping
        this->subpaths = new CT::Path::PathMapping(*path.subpaths);
    }
    // Copy the root path
    this->set_root(path.root);
}

//--------------------------------------------------
// PUBLIC METHODS (for PathMapping)
//--------------------------------------------------

CT::Path::PathMapping::PathMapping(const CT::Path::PathMapping &)
{
    std::map<char *, CT::Path *>::iterator it;
    for (it = this->begin(); it != this->end(); it++) {
        // Create a new path object by triggering the copy constructor
        CT::Path *val = new CT::Path(*it->second);
        this->add_path(val);
    }
}

CT::Path::PathMapping::~PathMapping()
{
    Path *val = NULL;
    std::map<char *, Path *>::iterator it;
    // Remove the subpaths member
    // iterate over the subpaths and destroy all paths
    while (!this->empty()) {
        it = this->begin();
        val = it->second;
        // Remove the element from the map
        this->erase(it);
        // Now we can remove the memory
        // NOTE: we only delete the Path here. The key is the same as the 'root' member
        //       Since destroying the path will destroy the root member as well,
        //       there is no need to explicitly destroy the key here.
        delete val;
    }
}

std::pair<CT::Path::PathMapping::iterator, bool> CT::Path::PathMapping::add_path(Path *path)
{
    return this->insert(std::pair<char *, Path *>(path->root, path));
}

//--------------------------------------------------
// PUBLIC METHODS (for Path)
//--------------------------------------------------

// Default constructor
CT::Path::Path(const char *root, const struct stat *st) :
    mode(0),
    size(0),
    scan_finished(false),
    calc_finished(false),
    root(NULL),
    subpaths(NULL)
{
    struct stat _st = {0};
    // Stat the path that was passed in and exit if
    // the path does not exist or is not a directory
    if (!st) {
        if (lstat(root, &_st)) {
            PRINTLN("Could not stat %s", root);
            exit(1);
        }
    } else {
        _st = *st;
    }
    // Make sure we are dealing with either a directory, file or link
    if (IS_UNSUPPORTED(_st.st_mode)) {
        PRINTLN("%s is not a valid path. Exiting...", root);
        exit(1);
    }
    // Now we start the actual object construction
    this->set_root(root);
    this->mode = _st.st_mode;
    if (S_ISDIR(this->mode)) {
        this->subpaths = new CT::Path::PathMapping;
    } else {
        this->size = _st.st_size;
    }
}

// Copy constructor
CT::Path::Path(const Path &path) :
    mode(0),
    size(0),
    scan_finished(false),
    calc_finished(false),
    root(NULL),
    subpaths(NULL)
{
    this->copy_from(path);
}

// We need the assignment operator to handle assignments
CT::Path& CT::Path::operator=(const Path &path)
{
    this->copy_from(path);
    return *this;
}

// Destructor
CT::Path::~Path()
{
    // Destroy the subpaths map
    this->destroy_subpaths();
    // Remove the root member. NOTE: Same as removing keys from subpaths map.
    this->destroy_root();
}

// Simply returns a string representing the type of path
const char *CT::Path::get_type() const
{
    return TYPE_REPR(this->mode);
}

// Simply returns the root path for which this object is responsible
const char *CT::Path::get_root() const
{
    // TODO: we should actually be returning a copy of the root variable so that we negate tampering attempts.
    return this->root;
}

// Returns the subpaths mapping
CT::Path::PathMapping *get_subpaths()
{
    return NULL;
}

// Size of the path
off_t CT::Path::get_size()
{
    // scan the directory and populate the subpaths
    this->scan_path();
    this->calc_size();
    // That's it, size should be updated now.
    return this->size;
}
